## Question 4 (Git)

### A

Quels sont les différences entre un _merge_ et un _rebase_? Expliquez comment il est possible de perdre du code avec les deux méthodes. 


### B 


Quand vous avez fini de répondre aux questions précédentes, commitez vos modifications en utilisant un message de commit significatif. Avant de pousser votre code, _rebasez_-vous sur master. Ensuite, poussez votre nouveau code sur la branche. 
